# vim: ts=2
{
	min = max = $1
	for (i=2; i<=NF; ++i) {
		min = $i < min ? $i : min
		max = $i > max ? $i : max
	}
	checksum += max - min
}

END {
	print checksum
}
