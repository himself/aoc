# vim: ts=2

function anagram(x, y,	i, j, c, rem) {
	if (length(x)!=length(y))
		return 0

	rem = length(y)
	split(y, c, "")
	for (i=1; i<=length(x); ++i)
		for (j=1; j<=length(y); ++j)
			if (substr(x, i, 1) == c[j]) {
				delete c[j]
				--rem
				break
			}

	return rem == 0
}

BEGIN {
	n = 0
}

{
	for (i=1; i<NF; ++i)
		for (j=i+1; j<=NF; ++j)
			if (anagram($i, $j))
				next

	++n
}

END {
	print n
}
