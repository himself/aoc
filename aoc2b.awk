# vim: ts=2
{
	for (i=1; i<NF; ++i) {
		for (j=i+1; j<=NF; ++j) {
			if ($i > $j) {
				a = $i; b = $j
			} else {
				a = $j; b = $i
			}
			if (a%b==0) {
				checksum += a/b
				next
			}
		}
	}
}

END {
	print checksum
}
