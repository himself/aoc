# vim: ts=2

function search(x,	k, sum) {
	w = weight[x]
	for (k=top[x]; k; k=sib[k])
		w += search(succ[k])
	return w
}

{
	x = $1
	weight[x] = substr($2, 2, length($2)-2)
	if(i = index($0, " -> ")) {
		split(substr($0, i+4), succ2, ", ")
		for (s in succ2) {
			pred[succ2[s]] = x
			succ[++k] = succ2[s]
			sib[k] = top[x]
			top[x] = k
		}
	}
}

END {
	while(pred[x])
		x = pred[x]
	search(x)
}
