# vim: ts=2

function abs(i) { return i < 0 ? -i : i }

{
	x = y = 0
	values[x,y] = 1
	vec = 1

	if (value[x,y]>$1)
		exit

	while(1) {
		delta = ((vec<0) ? -1 : 1)
		for(j=1; j<=abs(vec); ++j) {
			x += delta
			values[x,y] += values[x+1,y]
			values[x,y] += values[x+1,y+1]
			values[x,y] += values[x,y+1]
			values[x,y] += values[x-1,y+1]
			values[x,y] += values[x-1,y]
			values[x,y] += values[x-1,y-1]
			values[x,y] += values[x,y-1]
			values[x,y] += values[x+1,y-1]
			if(values[x,y]>$1) exit
		}
		for(j=1; j<=abs(vec); ++j) {
			y += delta
			values[x,y] += values[x+1,y]
			values[x,y] += values[x+1,y+1]
			values[x,y] += values[x,y+1]
			values[x,y] += values[x-1,y+1]
			values[x,y] += values[x-1,y]
			values[x,y] += values[x-1,y-1]
			values[x,y] += values[x,y-1]
			values[x,y] += values[x+1,y-1]
			if(values[x,y]>$1) exit
		}
		vec += delta
		vec = -vec
	}
}

END {
	print values[x,y]
}
