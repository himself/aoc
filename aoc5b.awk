# vim: ts=2

BEGIN {
	RS=""; FS="\n"
}

{
	n = 0
	i = 1
	while(i<=NF) {
		offset = $i
		if (offset >= 3)
			--$i
		else
			++$i
		i += offset
		++n
	}
}

END {
	print n
}
