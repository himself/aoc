# vim: ts=2

function abs(i) { return i < 0 ? -i : i }

{
	n = 1
	x = y = 0
	vec = 1

	if (n>=$1)
		exit

	while(1) {
		delta = ((vec<0) ? -1 : 1)
		for(j=1; j<=abs(vec); ++j) {
			x += delta
			++n
			if (n==$1) exit
		}
		for(j=1; j<=abs(vec); ++j) {
			y += delta
			++n
			if (n==$1) exit
		}
		vec += delta
		vec = -vec
	}
}

END {
	print abs(x)+abs(y)
}
