# vim: ts=2

BEGIN {
	RS=""; FS="\n"
}

{
	n = 0
	i = 1
	while(i<=NF) {
		offset = $i
		++$i
		i += offset
		++n
	}
}

END {
	print n
}
