# vim: ts=2

{
	x = $1
	if(i = index($0, " -> ")) {
		split(substr($0, i+4), succ, ", ")
		for (s in succ)
			pred[succ[s]] = x
	}
}

END {
	while(pred[x])
		x = pred[x]
	print x
}
