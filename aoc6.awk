# vim: ts=2
{
	while(1) {
		if ($0 in set)
			exit
		set[$0] = 1

		i = 1
		b = $1
		for(j=2; j<=NF; ++j)
			if ($j > b) {
				b = $j
				i = j
			}

		$i = 0
		while(b>0) {
			i = i%NF+1
			++$i
			--b
		}
		++n
	}
}

END {
	print n
}
