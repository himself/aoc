# vim: ts=2

{
	delete x

	for(i=1; i<=NF; ++i)
		x[$i] += 1

	for (key in x)
		if (x[key] > 1)
			next

	++n
}

END {
	print n
}
